package com.android.biodigester;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by user on 27-10-2017.
 */
public class Util {
    private static final String TAG = Util.class.getSimpleName();

    public static void showMessageAlertDialog(final Context context, String message) {
        try {
            LayoutInflater li = LayoutInflater.from(context);
            View promptsView = li.inflate(R.layout.prompt_message, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setView(promptsView);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            TextView lblMessage = (TextView) promptsView.findViewById(R.id.lblMessage);
            lblMessage.setText(message);
            Button btnOK = (Button) promptsView.findViewById(R.id.btnOK);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context,MainActivity.class);
                    context.startActivity(i);
                }
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "error message: " + e.getMessage());
        }
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static void showMessageAlertDialog(final Context mContext, String message, final String Page) {
        try {
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.prompt_message, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setView(promptsView);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            TextView lblMessage = (TextView) promptsView.findViewById(R.id.lblMessage);
            lblMessage.setText(message);
            Button btnOK = (Button) promptsView.findViewById(R.id.btnOK);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.cancel();
                    /**/
                    if(Page.equalsIgnoreCase("FinalActivity")){
                        Intent i = new Intent(mContext,ROIActivity.class);
                        mContext.startActivity(i);
                    }else if(Page.equalsIgnoreCase("ROIActivity")){
                        Intent i = new Intent(mContext,MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
                        mContext.startActivity(i);
                    }


                }
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
