package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ThirdActivity extends Activity {

    Button BtnNxt;
    EditText edtExpenseWasteDisposal,edtNumOfCylinders,edtDailyLPGConsumption;
    EditText spaceAvailability,preferredApplication,dieselCapacity,keroseneCapacity,petrolCapacity,gasCapacity;
    EditText dieselPhase,kerosenePhase,petrolPhase,gasPhase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_third);
            BtnNxt = (Button) findViewById(R.id.BtnNxt);

            edtExpenseWasteDisposal = (EditText) findViewById(R.id.expense_et);
            edtNumOfCylinders = (EditText) findViewById(R.id.cylinder_et);
            edtDailyLPGConsumption = (EditText) findViewById(R.id.consumption_et);

            spaceAvailability = (EditText) findViewById(R.id.space_et);
            preferredApplication = (EditText) findViewById(R.id.preferred_et);
            dieselCapacity = (EditText) findViewById(R.id.diesel_cap_et);
            keroseneCapacity = (EditText) findViewById(R.id.kerosene_cap_et);
            petrolCapacity = (EditText) findViewById(R.id.petrol_cap_et);
            gasCapacity = (EditText) findViewById(R.id.gas_cap_et);
            dieselPhase = (EditText) findViewById(R.id.diesel_phase_et);
            kerosenePhase = (EditText) findViewById(R.id.kerosene_phase_et);
            petrolPhase = (EditText) findViewById(R.id.petrol_phase_et);
            gasPhase = (EditText) findViewById(R.id.gas_phase_et);

            BtnNxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(writeData()){
                        Intent i = new Intent(ThirdActivity.this,FinalActivity.class);
                        startActivity(i);
                    }else{
                        Toast.makeText(ThirdActivity.this, "Please provide details", Toast.LENGTH_SHORT).show();
                    }


                   /* SharedPreferences.Editor mm = MainActivity.mSharedPreferences.edit();
                    mm.putFloat("expense_et",Float.parseFloat(edtExpenseWasteDisposal.getText().toString()));
                    mm.putFloat("cylinder_et",Float.parseFloat(edtNumOfCylinders.getText().toString()));
                    mm.putFloat("consumption_et",Float.parseFloat(edtDailyLPGConsumption.getText().toString()));

                    mm.commit();*/

                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private boolean writeData() {
        MyObject myObject = MainActivity.myObject;
        if(spaceAvailability.getText().toString().trim().length() !=0) {
            myObject.setSpaceAvailability(spaceAvailability.getText().toString());
        }else{
            return  false;
        }
        if(preferredApplication.getText().toString().trim().length() !=0) {
            myObject.setPreferredApplication(preferredApplication.getText().toString().trim());
        }else{
            return  false;
        }
        if(dieselCapacity.getText().toString().trim().length() !=0) {
            myObject.setDieselCapacity(dieselCapacity.getText().toString().trim());
        }else{
            return  false;
        }
        if(keroseneCapacity.getText().toString().trim().length() !=0) {
            myObject.setKeroseneCapacity(keroseneCapacity.getText().toString().trim());
        }else{
            return  false;
        }
        if(petrolCapacity.getText().toString().trim().length() !=0) {
            myObject.setPetrolCapacity(petrolCapacity.getText().toString().trim());
        }else{
            return  false;
        }
        if(gasCapacity.getText().toString().trim().length() !=0) {
            myObject.setGasCapacity(gasCapacity.getText().toString().trim());
        }else{
            return  false;
        }
        if(dieselPhase.getText().toString().trim().length() !=0) {
            myObject.setDieselPhase(dieselPhase.getText().toString().trim());
        }else{
            return  false;
        }
        if(kerosenePhase.getText().toString().trim().length() !=0) {
            myObject.setKerosenePhase(kerosenePhase.getText().toString().trim());
        }else{
            return  false;
        }
        if(petrolPhase.getText().toString().trim().length() !=0) {
            myObject.setPetrolPhase(petrolPhase.getText().toString().trim());
        }else{
            return  false;
        }
        if(gasPhase.getText().toString().trim().length() !=0) {
            myObject.setGasPhase(gasPhase.getText().toString().trim());
        }else{
            return  false;
        }

        if(edtExpenseWasteDisposal.getText().toString().trim().length() !=0) {
            myObject.setExpenseWasteDisposal(edtExpenseWasteDisposal.getText().toString().trim());
        }else{
            return  false;
        }
        if(edtNumOfCylinders.getText().toString().trim().length() !=0) {
            myObject.setNumOfCylinders(edtNumOfCylinders.getText().toString().trim());
        }else{
            return  false;
        }
        if(edtDailyLPGConsumption.getText().toString().trim().length() !=0) {
            myObject.setDailyLPGConsumption(edtDailyLPGConsumption.getText().toString().trim());
        }else{
            return  false;
        }
        return true;
    }
}
