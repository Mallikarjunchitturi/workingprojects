package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splash);

            final TextView textview = (TextView) findViewById(R.id.textview);
            final Animation animation_1 = AnimationUtils.loadAnimation(SplashActivity.this,R.anim.rotate);
            Animation animation_2 = AnimationUtils.loadAnimation(SplashActivity.this,R.anim.antirotate);
            final Animation animation_3 = AnimationUtils.loadAnimation(SplashActivity.this,R.anim.abc_fade_out);

            textview.startAnimation(animation_2);
            animation_2.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    textview.startAnimation(animation_1);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            animation_1.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    textview.startAnimation(animation_3);
                    Intent i = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(i);
                    finish();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
