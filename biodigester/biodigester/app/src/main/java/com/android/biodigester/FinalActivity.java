package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

public class FinalActivity extends Activity {

    Button BtnSubmit;
    EditText edtHotWater,edtName,edtPosition;//signature_edit1,signature_edit2,signature_edit3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_final);

            edtHotWater = (EditText) findViewById(R.id.signature_edit1);
            edtName = (EditText) findViewById(R.id.signature_edit2);
            edtPosition = (EditText) findViewById(R.id.signature_edit3);

            BtnSubmit = (Button) findViewById(R.id.BtnSubmit);
            final SignaturePad signaturePad = (SignaturePad) findViewById(R.id.signaturePad);
            ImageView mImageView = (ImageView) findViewById(R.id.signature_image);
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signaturePad.clear();
                }
            });

            BtnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(getData()){
                        Util.showMessageAlertDialog(FinalActivity.this,"Redirecting to ROI Screen","FinalActivity");
                    }else{
                        Toast.makeText(FinalActivity.this, "please provide valid data", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        catch (Exception e){

        }
    }

    private boolean getData() {

        MyObject myObject = MainActivity.myObject;

        myObject = new MyObject();
        if(edtHotWater.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setHotWaterUsage(edtHotWater.getText().toString().trim());
        }
        if(edtName.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setPersonName(edtName.getText().toString().trim());
        }
        if(edtPosition.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setPersonPosition(edtPosition.getText().toString().trim());
        }

        return true;
    }


    private void calculate(){

        // Calculation on waste to gas and power generation
            float totalWaste = MainActivity.mSharedPreferences.getFloat("totalWaste",0.0f);
        // Bio gas generation
            float gasProduction = totalWaste*(0.6f);
            float lpgEquilent   = gasProduction*(0.5f);
        // Power generation Calculation
            float powerGenaration =  gasProduction*2;
            float electricityEquilent = powerGenaration*5.8f;

        int LPG_packSize = 19;
        float LPG_Price = 1106.50f;
        int numDays = 30;
        int LPG_Consumption = 60;
        float costPerKG_LPG = LPG_Price/LPG_packSize;
        float totalMonthlyLPGCost = LPG_Price*LPG_Consumption;
        float dailyMitigation_LPG = lpgEquilent;
        float dailyMitigation_LPGCost = lpgEquilent*costPerKG_LPG;

        float monthlyMittigation_BioGas = dailyMitigation_LPGCost * numDays;
        float monthlyWasteDisposalCost= MainActivity.mSharedPreferences.getFloat("expense_et",0.0f);

        float totalMittigationCost = monthlyMittigation_BioGas + monthlyWasteDisposalCost;

        float capitalCost = 0.0f;

        float ROI_Months = capitalCost/totalMittigationCost;








    }

}
