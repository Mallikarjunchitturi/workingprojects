package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by MALLIKARJUN on 11/20/2017.
 */

public class ROIActivity extends Activity {

    TextView txtDetails;
    Button btnCalculateROI, btnSubmit, btnEdit;
    EditText edtAvgFWaste, edtLpgPackSize, edtLpgPackPrice_Month, edtLpgConsumption_Month, edtWasteDCM_Month, edtCapitalCost, edtROI;
    float avgFWaste,lpgPackSize,lpgPackPrice_Month,lpgConsumption_Month,wasteDCM_Month,capitalCost,roi;

    TextView txtAFWD,txtLPS,txtLPPFM,txtNDM,txtMLC,txtCPKL,txtTMLC,txtDML,txtDMC,txtMCM,txtMWDCM,txtTCM,txtCCFSGBP,txtROI;
    final int DAYS_IN_MONTH = 30;
    LinearLayout linear7;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.riscreen);

        txtDetails = (TextView) findViewById(R.id.txtDetails);
        txtDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMessageAlertDialog();
            }
        });

        edtAvgFWaste = (EditText) findViewById(R.id.edtAvgFWaste);
        edtLpgPackSize = (EditText) findViewById(R.id.edtLpgPackSize);
        edtLpgPackPrice_Month = (EditText) findViewById(R.id.edtLpgPackPrice_Month);
        edtLpgConsumption_Month = (EditText) findViewById(R.id.edtLpgConsumption_Month);
        edtWasteDCM_Month = (EditText) findViewById(R.id.edtWasteDCM_Month);
        edtCapitalCost = (EditText) findViewById(R.id.edtCapitalCost);
        edtROI = (EditText) findViewById(R.id.edtROI);

        btnCalculateROI = (Button) findViewById(R.id.btnROI);
        btnSubmit = (Button) findViewById(R.id.BtnSubmit);
        btnEdit = (Button) findViewById(R.id.BtnEdit);

        linear7 = (LinearLayout) findViewById(R.id.linear7);

        MyObject myObject = MainActivity.myObject;

        float totalWaste = (myObject.getKitchenWaste()) +
                            (myObject.getGardenWaste()) +
                            (myObject.getOtherWaste());

        edtAvgFWaste.setText(""+Util.round(totalWaste,2));

        btnCalculateROI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean result = validate();
                if(result) {
                    calculate();
                }else{
                    Toast.makeText(getApplicationContext(),"Please provide details",Toast.LENGTH_LONG).show();
                }
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btnCalculateROI.getVisibility() == View.VISIBLE){
                    Toast.makeText(ROIActivity.this,"please re calculate ROI",Toast.LENGTH_LONG).show();
                }else{
                    Util.showMessageAlertDialog(ROIActivity.this,"uploading data into server","ROIActivity");
                    /*Toast.makeText(ROIActivity.this,"uploading data into server",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(ROIActivity.this,MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();*/
                }
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCalculateROI.setVisibility(View.VISIBLE);
                //edtAvgFWaste.setEnabled(true);
                edtLpgPackSize.setEnabled(true);
                edtLpgPackPrice_Month.setEnabled(true);
                edtLpgConsumption_Month.setEnabled(true);
                edtWasteDCM_Month.setEnabled(true);
                edtCapitalCost.setEnabled(true);
                linear7.setVisibility(View.INVISIBLE);
            }
        });




    }

    public boolean validate(){

        if(edtAvgFWaste.getText().toString().length() != 0){
            avgFWaste = Float.parseFloat(edtAvgFWaste.getText().toString());
        }else{
            return false;
        }
        if(edtLpgPackSize.getText().toString().length() != 0){
            lpgPackSize = Float.parseFloat(edtLpgPackSize.getText().toString());
        }else{
            return false;
        }
        if(edtLpgPackPrice_Month.getText().toString().length() != 0){
            lpgPackPrice_Month = Float.parseFloat(edtLpgPackPrice_Month.getText().toString());
        }else{
            return false;
        }
        if(edtLpgConsumption_Month.getText().toString().length() != 0){
            lpgConsumption_Month = Float.parseFloat(edtLpgConsumption_Month.getText().toString());
        }else{
            return false;
        }
        if(edtWasteDCM_Month.getText().toString().length() != 0){
            wasteDCM_Month = Float.parseFloat(edtWasteDCM_Month.getText().toString());
        }else{
            return false;
        }
        if(edtCapitalCost.getText().toString().length() != 0){
            capitalCost = Float.parseFloat(edtCapitalCost.getText().toString());
        }else{
            return false;
        }
       /* if(edtROI.getText().toString().length() == 0){
            roi = Float.parseFloat(edtROI.getText().toString());
        }else{
            return false;
        }*/

        return true;
    }

    public void showMessageAlertDialog() {
        try {
            LayoutInflater li = LayoutInflater.from(ROIActivity.this);
            View promptsView = li.inflate(R.layout.details_layout, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ROIActivity.this);
            alertDialogBuilder.setView(promptsView);
            final AlertDialog alertDialog = alertDialogBuilder.create();
           /* TextView lblMessage = (TextView) promptsView.findViewById(R.id.lblMessage);
            lblMessage.setText(message); */

            txtAFWD = (TextView)promptsView.findViewById(R.id.txtAFWD);
            txtLPS = (TextView) promptsView.findViewById(R.id.txtLPS);
            txtLPPFM = (TextView) promptsView.findViewById(R.id.txtLPPFM);
            txtNDM = (TextView) promptsView.findViewById(R.id.txtNDM);
            txtMLC = (TextView) promptsView.findViewById(R.id.txtMLC);
            txtCPKL = (TextView) promptsView.findViewById(R.id.txtCPKL);
            txtTMLC = (TextView) promptsView.findViewById(R.id.txtTMLC);
            txtDML = (TextView) promptsView.findViewById(R.id.txtDML);
            txtDMC = (TextView) promptsView.findViewById(R.id.txtDMC);

            txtMCM = (TextView) promptsView.findViewById(R.id.txtMCM);
            txtMWDCM = (TextView) promptsView.findViewById(R.id.txtMWDCM);
            txtTCM = (TextView) promptsView.findViewById(R.id.txtTCM);
            txtCCFSGBP = (TextView) promptsView.findViewById(R.id.txtCCFSGBP);
            txtROI = (TextView) promptsView.findViewById(R.id.txtROI);


            ImageView imgCancel = (ImageView) promptsView.findViewById(R.id.imgCancel);
            imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.cancel();
                    /**/

                }
            });
            alertDialog.show();

            txtAFWD.setText(""+Util.round(avgFWaste,2));
            txtLPS.setText(""+Util.round(lpgPackSize,2));
            txtLPPFM.setText(""+Util.round(lpgPackPrice_Month,2));
            txtMLC.setText(""+Util.round(lpgConsumption_Month,2));

            float perKG = lpgPackPrice_Month/lpgPackSize;
            txtCPKL.setText(""+Util.round(perKG,2));
            txtTMLC.setText(""+Util.round((perKG*DAYS_IN_MONTH),2));

            float dailtyMitigation = calculateMitigation();
            txtDML.setText(""+Util.round(dailtyMitigation,2));
            float dailtyMitigationCost = (perKG * dailtyMitigation);
            txtDMC.setText(""+Util.round(dailtyMitigationCost,2));  //daily mitigation
            float monthlyMCost = dailtyMitigationCost * DAYS_IN_MONTH;
            txtMCM.setText(""+Util.round(monthlyMCost,2));
            txtMWDCM.setText(""+Util.round(wasteDCM_Month,2));
            float totalMCost = wasteDCM_Month+monthlyMCost;
            txtTCM.setText(""+Util.round(totalMCost,2));
            txtCCFSGBP.setText(""+Util.round(capitalCost,2));

            float ROI = (capitalCost/totalMCost);
            txtROI.setText(""+Util.round(ROI,2));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void calculate(){

        float dailtyMitigation = calculateMitigation();
        float perKG = lpgPackPrice_Month/lpgPackSize;
        float dailtyMitigationCost = (perKG * dailtyMitigation);
        float monthlyMCost = dailtyMitigationCost * DAYS_IN_MONTH;
        float totalMCost = wasteDCM_Month+monthlyMCost;
        float ROI = (capitalCost/totalMCost);

        edtROI.setText(""+Util.round(ROI,2));
        btnSubmit.setVisibility(View.VISIBLE);
        btnEdit.setVisibility(View.VISIBLE);
        btnCalculateROI.setVisibility(View.INVISIBLE);

        //edtAvgFWaste.setEnabled(false);
        edtLpgPackSize.setEnabled(false);
        edtLpgPackPrice_Month.setEnabled(false);
        edtLpgConsumption_Month.setEnabled(false);
        edtWasteDCM_Month.setEnabled(false);
        edtCapitalCost.setEnabled(false);
        linear7.setVisibility(View.VISIBLE);

    }

    public  float calculateMitigation(){

        //float mitigation = 0.0f;

        //Calculation on waste to gas and power genera



        MyObject myObject = MainActivity.myObject;

        float totalWaste = (myObject.getKitchenWaste() * 0.20f) +
                           (myObject.getGardenWaste() * 0.20f) +
                           (myObject.getOtherWaste() * 0.80f);


        //Bio gas generation

        float totalOrganicWaste = totalWaste * 0.6f;
        float totalLPGEquilent = totalOrganicWaste * 0.5f; // KGS

        //Power generation Calculation
        float totalAmountOfGas = totalOrganicWaste * 2; // kwh
        float electricityEquilent = totalAmountOfGas * 5.8f; // rupees

        return totalLPGEquilent;
    }
}
