package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.android.biodigester.MainActivity.mSharedPreferences;

public class SecondActivity extends Activity {

    Button BtnNxt;
    EditText edtAnimalWastePig,edtAnimalWasteSheep,edtAnimalWasteCow,edtAnimalWasteCattle,edtAnimalWasteChicken;
    EditText edtKitchenWaste,edtGardenWaste,edtOtherWaste;
    EditText edtWetWasteDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_second);

            BtnNxt = (Button) findViewById(R.id.BtnNxt);
            edtKitchenWaste = (EditText) findViewById(R.id.kday_et);
            edtGardenWaste = (EditText) findViewById(R.id.gweek_et);
            edtOtherWaste = (EditText) findViewById(R.id.others_et);

            edtAnimalWastePig = (EditText) findViewById(R.id.pig_et);
            edtAnimalWasteSheep = (EditText) findViewById(R.id.sheep_et);
            edtAnimalWasteCow = (EditText) findViewById(R.id.cow_et);
            edtAnimalWasteCattle = (EditText) findViewById(R.id.cattle_et);
            edtAnimalWasteChicken = (EditText) findViewById(R.id.chicken_et);
            edtWetWasteDetails = (EditText) findViewById(R.id.wetWastePractice_et);

            BtnNxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                if(getData()){
                    Intent i = new Intent(SecondActivity.this,ThirdActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(SecondActivity.this, "please provide valid data", Toast.LENGTH_SHORT).show();
                }
                }
            });

        }
        catch (Exception e)
        {

        }

    }

    private boolean getData() {
        MyObject myObject = MainActivity.myObject;
        if(edtAnimalWastePig.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setAnimalWastePig(edtAnimalWastePig.getText().toString());

        if(edtAnimalWasteSheep.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setAnimalWasteSheep(edtAnimalWasteSheep.getText().toString());

        if(edtAnimalWasteCow.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setAnimalWasteCow(edtAnimalWasteCow.getText().toString());

        if(edtAnimalWasteCattle.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setAnimalWasteCattle(edtAnimalWasteCattle.getText().toString());

        if(edtAnimalWasteChicken.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setAnimalWasteChicken(edtAnimalWasteChicken.getText().toString());

        if(edtKitchenWaste.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setKitchenWaste(Float.parseFloat(edtKitchenWaste.getText().toString()));

        if(edtGardenWaste.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setGardenWaste(Float.parseFloat(edtGardenWaste.getText().toString()));

        if(edtOtherWaste.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setOtherWaste(Float.parseFloat(edtOtherWaste.getText().toString()));

        if(edtWetWasteDetails.getText().toString().trim().length() == 0){
            return false;
        }
        myObject.setWetWasteDetails(edtWetWasteDetails.getText().toString());


        return  true;
    }
}
