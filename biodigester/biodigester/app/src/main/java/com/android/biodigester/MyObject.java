package com.android.biodigester;

import android.widget.EditText;

/**
 * Created by MALLIKARJUN on 11/24/2017.
 */

public class MyObject {

    MyObject(){

    }
    String hotelName,contactNumber,telePhoneNumber,email,corporate,site;
    String animalWastePig,animalWasteSheep,animalWasteCow,animalWasteCattle,animalWasteChicken,wetWasteDetails;
    String spaceAvailability,preferredApplication,dieselCapacity,keroseneCapacity,petrolCapacity,gasCapacity;
    String dieselPhase,kerosenePhase,petrolPhase,gasPhase;
    String expenseWasteDisposal,numOfCylinders,dailyLPGConsumption;
    String hotWaterUsed,name,position;//edtHotWater,edtName,edtPosition;
    String hotWaterUsage,personName,personPosition;//edtHotWater,edtName,edtPosition;

    float kitchenWaste,gardenWaste,otherWaste;

    public void setHotelName(String hotelName){
        this.hotelName = hotelName;
    }
    public String getHotelName(){
        return hotelName;
    }

    public void setTelePhoneNumber(String telePhoneNumber){
        this.telePhoneNumber = telePhoneNumber;
    }
    public String getTelePhoneNumber(){
        return telePhoneNumber;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber = hotelName;
    }
    public String getContactNumber(){
        return contactNumber;
    }

    public void setEmail(String email){
        this.email = email;
    }
    public String getEmail(){
        return email;
    }

    public void setCorporate(String corporate){
        this.corporate = corporate;
    }
    public String getCorporate(){
        return corporate;
    }

    public void setSite(String site){
        this.site = site;
    }
    public String getSite(){
        return site;
    }

    public void setAnimalWastePig(String animalWastePig){
        this.animalWastePig = animalWastePig;
    }
    public String getAnimalWastePig(){
        return animalWastePig;
    }

    public void setAnimalWasteSheep(String animalWasteSheep){
        this.animalWasteSheep = animalWasteSheep;
    }
    public String getAnimalWasteSheep(){
        return animalWasteSheep;
    }

    public void setAnimalWasteCow(String animalWasteCow){
        this.animalWasteCow = animalWasteCow;
    }
    public String getAnimalWasteCow(){
        return animalWasteCow;
    }

    public void setAnimalWasteCattle(String animalWasteCattle){
        this.animalWasteCattle = animalWasteCattle;
    }
    public String getAnimalWasteCattle(){
        return animalWasteCattle;
    }

    public void setAnimalWasteChicken(String animalWasteChicken){
        this.animalWasteChicken = animalWasteChicken;
    }
    public String getAnimalWasteChicken(){
        return animalWasteChicken;
    }

    public void setKitchenWaste(float kitchenWaste){
        this.kitchenWaste = kitchenWaste;
    }
    public float getKitchenWaste(){
        return kitchenWaste;
    }

    public void setGardenWaste(float gardenWaste){
        this.gardenWaste = gardenWaste;
    }
    public float getGardenWaste(){
        return gardenWaste;
    }

    public void setOtherWaste(float otherWaste){
        this.otherWaste = otherWaste;
    }
    public float getOtherWaste(){
        return otherWaste;
    }

    public void setWetWasteDetails(String wetWasteDetails){
        this.wetWasteDetails = wetWasteDetails;
    }
    public String getWetWasteDetails(){
        return wetWasteDetails;
    }

    public void setDieselPhase(String dieselPhase){
        this.dieselPhase = dieselPhase;
    }
    public String getDieselPhase(){
        return dieselPhase;
    }
    public void setKerosenePhase(String kerosenePhase){
        this.kerosenePhase = kerosenePhase;
    }
    public String getKerosenePhase(){
        return kerosenePhase;
    } public void setPetrolPhase(String petrolPhase){
        this.petrolPhase = petrolPhase;
    }
    public String getPetrolPhase(){
        return petrolPhase;
    }
    public void setGasPhase(String gasPhase){
        this.gasPhase = gasPhase;
    }
    public String getGasPhase(){
        return gasPhase;
    }

    public void setSpaceAvailability(String spaceAvailability){
        this.spaceAvailability = spaceAvailability;
    }
    public String getSpaceAvailability(){
        return spaceAvailability;
    }
    public void setPreferredApplication(String preferredApplication){
        this.preferredApplication = preferredApplication;
    }
    public String getPreferredApplication(){
        return preferredApplication;
    }
    public void setDieselCapacity(String dieselCapacity){
        this.dieselCapacity = dieselCapacity;
    }
    public String getDieselCapacity(){
        return dieselCapacity;
    }
    public void setKeroseneCapacity(String keroseneCapacity){
        this.keroseneCapacity = keroseneCapacity;
    }
    public String getKeroseneCapacity(){
        return keroseneCapacity;
    }
    public void setPetrolCapacity(String petrolCapacity){
        this.petrolCapacity = petrolCapacity;
    }
    public String getPetrolCapacity(){
        return petrolCapacity;
    }
    public void setGasCapacity(String gasCapacity){
        this.gasCapacity = gasCapacity;
    }
    public String getGasCapacity(){
        return gasCapacity;
    }

    public void setExpenseWasteDisposal(String expenseWasteDisposal){
        this.expenseWasteDisposal = expenseWasteDisposal;
    }
    public String getExpenseWasteDisposal(){
        return expenseWasteDisposal;
    }
    public void setNumOfCylinders(String numOfCylinders){
        this.numOfCylinders = numOfCylinders;
    }
    public String getNumOfCylinders(){
        return numOfCylinders;
    }
    public void setDailyLPGConsumption(String dailyLPGConsumption){
        this.dailyLPGConsumption = dailyLPGConsumption;
    }
    public String getDailyLPGConsumption(){
        return dailyLPGConsumption;
    }

    public void setHotWaterUsed(String hotWaterUsed){
        this.hotWaterUsed = hotWaterUsed;
    }
    public String getHotWaterUsed(){
        return hotWaterUsed;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public void setPosition(String position){
        this.position = position;
    }
    public String getPosition(){
        return position;
    }

    public void setHotWaterUsage(String hotWaterUsage){
        this.hotWaterUsage = hotWaterUsage;
    }
    public String getHotWaterUsage(){
        return hotWaterUsage;
    }
    public void setPersonName(String personName){
        this.personName = personName;
    }
    public String getPersonName(){
        return personName;
    }
    public void setPersonPosition(String personPosition){
        this.personPosition = personPosition;
    }
    public String getPersonPosition(){
        return personPosition;
    }


}
