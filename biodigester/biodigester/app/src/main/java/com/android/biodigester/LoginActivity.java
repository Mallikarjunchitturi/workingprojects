package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class LoginActivity extends Activity {

    Button LoginBtn;
    private static final float BLUR_RADIUS = 25f;
    EditText edLoginID,edPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);

            LoginBtn = (Button) findViewById(R.id.LoginBtn);

            ImageView imageView = (ImageView) findViewById(R.id.bg_iv);
            edLoginID = (EditText) findViewById(R.id.edLoginID);
            edPassword = (EditText) findViewById(R.id.edPassword);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.bg_image);
            Bitmap blurredBitmap = blur(bitmap);
            imageView.setImageBitmap(blurredBitmap);

            LoginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userName = edLoginID.getText().toString().trim();
                    String password = edPassword.getText().toString();
                    if(userName.length() ==0 || password.length() ==0){
                        Toast.makeText(getApplicationContext(),"please provide valid credentials",Toast.LENGTH_LONG).show();
                    }else{
                        Intent i = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }

                }
            });
        }
        catch (Exception e){
                e.printStackTrace();
        }

    }

    public Bitmap blur(Bitmap image) {
        Bitmap outputBitmap = null;
        try {
            if (null == image) return null;

            outputBitmap = Bitmap.createBitmap(image);
            final RenderScript renderScript = RenderScript.create(this);
            Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
            Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

            //Intrinsic Gausian blur filter
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

        } catch (Exception e) {
            //Toast.makeText(LoginActivity.this,getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        }
        return outputBitmap;
    }
}
