package com.android.biodigester;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    Button BtnNxt;
    public static SharedPreferences mSharedPreferences;
    EditText edtHotel,edtContact,edtTel,edtEmail,edtCorporate,edtSite;
    public static MyObject myObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            mSharedPreferences = getSharedPreferences("WasteManagement",MODE_PRIVATE);
            BtnNxt = (Button) findViewById(R.id.BtnNxt);

            edtHotel = (EditText) findViewById(R.id.hotel_et);
            edtContact = (EditText) findViewById(R.id.contact_et);
            edtTel = (EditText) findViewById(R.id.tel_et);
            edtEmail = (EditText) findViewById(R.id.email_et);
            edtCorporate = (EditText) findViewById(R.id.corporate_et);
            edtSite = (EditText) findViewById(R.id.site_et);


            BtnNxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if(writeData()){
                       Intent i = new Intent(MainActivity.this,SecondActivity.class);
                       startActivity(i);
                   }else{
                       Toast.makeText(MainActivity.this, "Please provide details", Toast.LENGTH_SHORT).show();
                   }
                }
            });
        }
        catch (Exception e){

        }
    }

    private boolean writeData() {

        myObject = new MyObject();
        if(edtHotel.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setContactNumber(edtContact.getText().toString().trim());
        }
        if(edtTel.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setTelePhoneNumber(edtTel.getText().toString().trim());
        }
        if(edtEmail.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setEmail(edtEmail.getText().toString().trim());
        }
        if(edtCorporate.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setCorporate(edtCorporate.getText().toString().trim());
        }
        if(edtSite.getText().toString().trim().length() == 0){
            return  false;
        }else {
            myObject.setSite(edtSite.getText().toString().trim());
        }
        return  true;
    }
}
