package com.phonegap.test1;

import org.apache.cordova.engine.SystemWebViewClient;
import org.apache.cordova.engine.SystemWebViewEngine;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.widget.Toast;

public class myWebViewClient extends SystemWebViewClient  {

	public myWebViewClient(SystemWebViewEngine parentEngine) {
		super(parentEngine);
		// TODO Auto-generated constructor stub
	}
	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		// TODO Auto-generated method stub
		   if (!MainActivity.checkInternetConnection()) {
		      Toast.makeText(MainActivity.mContext, "Aww ! No Internet Connection ", Toast.LENGTH_SHORT).show();
		    } else {
		      view.loadUrl(url);
		    }  			   
         return true;
	}
	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		// TODO Auto-generated method stub
		try{
			MainActivity.launchRingDialog();
		}catch(Exception e){
			e.printStackTrace();
		}
		super.onPageStarted(view, url, favicon);
	}
	@Override
	public void onPageFinished(WebView view, String url) {
		// TODO Auto-generated method stub
		try {
			MainActivity.ringProgressDialog.dismiss();
			MainActivity.ringProgressDialog = null;
		} catch (Exception e) {

		}
		view.clearCache(true);
		super.onPageFinished(view, url);
	}
	@Override
	public void onPageCommitVisible(WebView view, String url) {
		// TODO Auto-generated method stub
		try {
			MainActivity.ringProgressDialog.dismiss();
			MainActivity.ringProgressDialog = null;
		} catch (Exception e) {

		}
		super.onPageCommitVisible(view, url);
	}
	

}
