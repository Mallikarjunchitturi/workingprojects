/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.phonegap.test1;

import org.apache.cordova.CordovaActivity;
import org.apache.cordova.engine.SystemWebViewEngine;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends CordovaActivity
{
	public static String url = "http://streetbuzz.co.in/newsapp";//test/test/index.html";
	
	String mUrl = "file:///android_asset/www/index.html";

	//WebView webview;
	private static final String TAG = "Main";
   // private ProgressDialog progressBar;  
    public static ProgressDialog ringProgressDialog = null;
   public static  Context mContext;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Set by <content src="index.html" /> in config.xml
        //loadUrl("file:///android_asset/www/test.html");
        super.init();
        //appView.loadUrl(url);
      // appView.showWebPage(url, false, true, null);
        mContext = this;
        
        SystemWebViewEngine systemWebViewEngine = (SystemWebViewEngine) appView.getEngine();
        WebViewClient myWebViewClient = new myWebViewClient(systemWebViewEngine);

        WebView webView = (WebView) systemWebViewEngine.getView();
        webView.setWebViewClient(myWebViewClient);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); 
        webView.clearCache(true);
        webView.loadUrl(url);
    }
    
    public  static void launchRingDialog() {
		// TODO Auto-generated method stub
		if(ringProgressDialog == null){
		ringProgressDialog = new ProgressDialog(mContext,R.style.StyledDialog);

		ringProgressDialog.setMax(100);
		ringProgressDialog.setMessage(" Loading....");
		ringProgressDialog.setIndeterminate(true);
		ringProgressDialog
				.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		ringProgressDialog.setCancelable(false);
		ringProgressDialog.show();
		}
	}
    public static boolean checkInternetConnection() {   

        ConnectivityManager con_manager = (ConnectivityManager) 
          mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (con_manager.getActiveNetworkInfo() != null
            && con_manager.getActiveNetworkInfo().isAvailable()
            && con_manager.getActiveNetworkInfo().isConnected()) {
          return true;
        } else {
          return false;
        }
      }
    
}
