package com.phonegap.test1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by MALLIKARJUN on 2/3/2018.
 */

public class SplashScreen extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Start home activity

setContentView(R.layout.splashscreen);
        final Handler handel = new Handler();
        handel.postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                // close splash activity
                finish();
            }
        }, 1000*3);
    }
}
