var app = angular.module('streetbusApp',['ngRoute','ngStorage']);
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/login', {templateUrl: 'login.html',controller:'loginController'})
        .when('/register', {templateUrl: 'register.html',controller:'registerController'})
        .when('/home', {templateUrl: 'home.html',controller:'homeController'})
		.when('/redirectlogin/:regResusername/:regRespassword', {templateUrl: 'redirectlogin.html',controller:'redirectloginController'})
		.when('/redirectregister/:regResuserid', {templateUrl: 'redirectregister.html',controller:'redirectregisterController'})
		.when('/redirect', {templateUrl: 'redirect.html',controller:'redirectController'})
		.when('/privacy', {templateUrl: 'privacy.html',controller:'privacyController'})
		.when('/terms', {templateUrl: 'terms.html',controller:'termsController'})
		.when('/offline', {templateUrl: 'offline.html',controller:'offlineController'})
		.when('/imgload', {templateUrl: 'imgload.html',controller:'imgloadController'}) /* NEW CONTROLLER */
        .otherwise({redirectTo: '/home'});
}]);  
app.run(function($window, $rootScope) {
    $rootScope.online = navigator.onLine;
    $window.addEventListener("offline", function () {
        $rootScope.$apply(function() {
            $rootScope.online = false;
        });
    }, false);
    $window.addEventListener("online", function () {
        $rootScope.$apply(function() {
            $rootScope.online = true;
        });
    }, false);
});  
app.controller('redirectloginController',function($scope,$http,$window,$localStorage,$location,$routeParams){ 
	var conStatusVal = $scope.online;												 
    if(conStatusVal){
		$scope.backToHome = function () {
            $location.path('/home');
        };
		$scope.usernameRegistration=$routeParams.regResusername; 
		$scope.passwordRegistration=$routeParams.regRespassword;        
		$window.open('http://www.streetbuzz.co.in/test/dashboard?mobuser='+$scope.usernameRegistration+'&mobpass='+$scope.passwordRegistration,'_self','location=no'); 
    }else{
        $location.path('/offline');
    }
 }); 
app.controller('redirectregisterController',function($scope,$http,$window,$localStorage,$location,$routeParams){ 
	var conStatusVal = $scope.online;												 
    if(conStatusVal){
		$scope.backToHome = function () {
            $location.path('/home');
        };
		$scope.useridRegistration=$routeParams.regResuserid;  
  		$window.open('http://www.streetbuzz.co.in/test/categeory_app?user_id='+$scope.useridRegistration,'_self','location=no'); 
    }else{
        $location.path('/offline');
    }
 }); 
app.controller('offlineController',function($scope,$http,$window,$localStorage,$location,$routeParams){
	//alert('Inside Offline Controller');										
	var conStatusVal = $scope.online; 
    if(conStatusVal){ 
        $scope.offlineContinue = function () { 
            $location.path('/');
        };
    }
 }); 
app.controller('imgloadController',function($scope,$http,$window,$localStorage,$location){
    console.log('Inside Load Controller');
 }); 
app.controller('privacyController',function($scope,$http,$window,$localStorage,$location){
    var conStatusVal = $scope.online;
	if(conStatusVal){
        $scope.privacytoregister = function () {
            $location.path('/register');
        };
    }else{
        $location.path('/offline');
    }
 }); 
app.controller('termsController',function($scope,$http,$window,$localStorage,$location){
    var conStatusVal = $scope.online;
	if(conStatusVal){
        $scope.termstoregister = function(){
            $location.path('/register');
        };
    }else{
        $location.path('/offline');
    }
 });  
app.controller('registerController',function($scope,$http,$window,$localStorage,$location){
    var conStatusVal = $scope.online;
	if(conStatusVal){
        $scope.registertologin = function () {
            $location.path('/login');
        };
        $scope.privacy = function () {
            $location.path('/privacy');
        };
        $scope.terms = function () {
            $location.path('/terms');
        };
        $scope.regtohome = function () {
            $location.path('/home');
        };
        $scope.register = function () {
            var streetbuzzLoggedStatus = 'N';
            var streetbuzzRegisterStatus = 'N';
            var streetbuzzRegisterusername = '';
            var streetbuzzRegisterpassword = '';
			var streetbuzzRegisteruserid = '';
            var validRegister = true;
            var fullname = $('#fullname').val().trim();
            var useremail = $('#useremail').val().trim();
            var userpassword = $('#userpassword').val().trim();
            var username = $('#regusername').val().trim();
            var birthdayday = $('#birthdayday').val().trim();
            var birthdaymonth = $('#birthdaymonth').val().trim();
            var birthdayyear = $('#birthdayyear').val().trim();
            var checkedCount = $('[name="profile_gender"]:checked').length;
            var checkedValue = $('[name="profile_gender"]:checked').val();
            if(fullname == "" || useremail == "" || userpassword == "" || username == "" || birthdayday == "" || birthdaymonth == "" || birthdayyear == "" || checkedCount <= 0){
                $('#errorRegister').text("All fields are required");
                validRegister = false;
            }
            if(validRegister){ 
                $('#errorRegister').text(" ");
                var userFormData = {'fullname':fullname,
                    'strret_useremail':useremail,
                    'street_userpassword':userpassword,
                    'street_username':username,
                    'profile_birth_day':birthdayday,
                    'profile_birth_month':birthdaymonth,
                    'profile_birth_year':birthdayyear,
                    'profile_gender':checkedValue,
                    'action':'R'
                };
                var reqURL = 'http://streetbuzz.co.in/test/userappcheck';
                var req = {
                    method: 'POST',
                    url: reqURL,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: userFormData
                }
                $http(req).success(function(response,status) { 
                    var regResusername =  response['username'];
                    var regRespassword =  response['password'];
					var regResuserid = response['user_id'];
                    var regResStatus =  response['status']; 
                    if(regResStatus){
                        streetbuzzRegisterStatus = 'Y';
                        streetbuzzLoggedStatus = 'Y';
                        streetbuzzRegisterusername = regResusername;
                        streetbuzzRegisterpassword = regRespassword;
						streetbuzzRegisteruserid = regResuserid;
                        $localStorage.StreetBuzzRegistered = streetbuzzRegisterStatus;
                        $localStorage.StreetBuzzLogged = streetbuzzLoggedStatus;
                        $localStorage.StreetBuzzUsername = streetbuzzRegisterusername;
                        $localStorage.StreetBuzzPassword = streetbuzzRegisterpassword;
						$localStorage.StreetBuzzUserID = streetbuzzRegisteruserid;
                        //$location.path('/redirectregister/'+regResusername+'/'+regRespassword);
						$location.path('/redirectregister/'+regResuserid);
                    }
                });
                $http(req).error(function(){
                    alert('Some error occured. Try Again.');
                });
            }
        }
    }else{
        $location.path('/offline');
    }
});  
app.controller('redirectController',function($scope,$http,$window,$localStorage,$location) {
    var conStatusVal = $scope.online;
	if(conStatusVal){
        var streetBuzzLogged = $localStorage.StreetBuzzLogged;
        var streetBuzzRegistered = $localStorage.StreetBuzzRegistered;
        var streetBuzzUsername = $localStorage.StreetBuzzUsername;
        var streetBuzzPassword = $localStorage.StreetBuzzPassword;
        if(typeof streetBuzzLogged === "undefined" && typeof streetBuzzRegistered === "undefined"){
            //alert(streetBuzzLogged);
            //$location.path('/register');
        }
    }else{
        $location.path('/offline');
    }
 });  
app.controller('homeController',function($scope,$http,$window,$localStorage,$location) {  
	var conStatusVal = $scope.online;
	if(conStatusVal)
    {
		var streetBuzzLogged = $localStorage.StreetBuzzLogged; 
		var streetBuzzRegistered = $localStorage.StreetBuzzRegistered; 
		var streetBuzzUsername = $localStorage.StreetBuzzUsername;
		var streetBuzzPassword = $localStorage.StreetBuzzPassword; 
		if(typeof streetBuzzLogged !== "undefined" && typeof streetBuzzRegistered !== "undefined" && streetBuzzLogged == 'Y' && streetBuzzRegistered == 'Y'){
			$('.txt-medium').hide();
			var regResusername = streetBuzzUsername;
			var regRespassword = streetBuzzPassword; 
			var userFormData = {'username':regResusername,'password':regRespassword,'action':'L'};
            var reqURL = 'http://streetbuzz.co.in/test/userappcheck';
            var req = {
                 method: 'POST',
                 url: reqURL,
                 headers: {
                     'Content-Type': 'application/json'
                 },
                 data: userFormData
            }
			$http(req).success(function(response,status) {
					var accountstatus = response['status'];
                    var accountusername = response['username'];
                    if(accountstatus){
                        regResusername = accountusername;
                        regRespassword = regRespassword;
                        $location.path('/redirectlogin/'+regResusername+'/'+regRespassword);
                    } 
			});
            $http(req).error(function(){
			 	alert('Some error occured. Try Again.');
            }); 
		} 
		$scope.homeLogin = function(){  
			$location.path('/login');
		};
		$scope.imgHELLO = function(){  
			//$location.path('/hello');
			//$window.open('http://192.168.1.100/test1/hello.php','_self','location=no');
			window.open = cordova.InAppBrowser.open;
var ref = window.open('http://192.168.1.101/test1/hello.php','_blank','location=no');
			//cordova.InAppBrowser.open();
			//ref.addEventListener('loadstart', function(event) { alert('start: ' + event.url); });  
			//ref.addEventListener('loadstop', function(event) { alert('stop: ' + event.url); });
			//ref.addEventListener('loaderror', function(event) { alert('error: ' + event.message); });
			//ref.addEventListener('exit', function(event) { alert(event.type); });
		};
		$scope.homeRegister = function(){  
			$location.path('/register');
		}; 
		$scope.homeLogout = function(){   
			var streetBuzzLogout;
			$localStorage.StreetBuzzLogged = streetBuzzLogout; 
			$localStorage.StreetBuzzRegistered = streetBuzzLogout; 
			$location.path('/');
		};
		$scope.imgLoad = function(){  
			$location.path('/imgload');
		};
    }
    else
    {
    	$location.path('/offline');
    }  
}); 
app.controller('loginController',function($scope,$http,$window,$localStorage,$location) {
    var conStatusVal = $scope.online; 
	if(conStatusVal){
        var streetBuzzLogged = $localStorage.StreetBuzzLogged;
        var streetBuzzRegistered = $localStorage.StreetBuzzRegistered;
        $scope.logintoregister =  function(){
            $location.path('/register');
        };
        $scope.login = function(){
            var streetbuzzLoggedStatus = 'N';
            var streetbuzzRegisterStatus = 'N';
            var streetbuzzRegisterusername = '';
            var streetbuzzRegisterpassword = '';
            var validLogin = true;
            var email = "";
            var password = "";
            var loggedin = 'F';
            var registered = 'F';
            $('#errorLogin').text("");
            email = $('#loginemail').val().trim();
            password = $('#loginpassword').val().trim();

            if(email == "" || password == ""){
                $('#errorLogin').text("Username and Password required");
                validLogin = false;
            }
            if(validLogin){
                $scope.email = email;
                $scope.password = password;
                var userFormData = {'username':$scope.email,'password':$scope.password,'action':'L'};
                var reqURL = 'http://streetbuzz.co.in/test/userappcheck';
                var req = {
                    method: 'POST',
                    url: reqURL,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: userFormData
                }
                $http(req).success(function(response,status) {
                    //alert(response);
                    var accountname = response['fullname'];
                    var accountpassword = response['password'];
                    var accountstatus = response['status'];
                    var accountusername = response['username'];
                    var accountuserlogged = 'N';
                    var accountuserregistered = 'N';
                    if(accountstatus){
                        streetbuzzLoggedStatus = 'Y';
                        streetbuzzRegisterStatus = 'Y';
                        regResusername = accountusername;
                        regRespassword = $scope.password;
                        $localStorage.StreetBuzzLogged = streetbuzzLoggedStatus;
                        $localStorage.StreetBuzzRegistered = streetbuzzRegisterStatus;
                        $localStorage.StreetBuzzUsername = regResusername;
                        $localStorage.StreetBuzzPassword = regRespassword;
                        $location.path('/redirectlogin/'+regResusername+'/'+regRespassword);
                    }
                });
                $http(req).error(function(){
                    alert('Some error occured. Try Again.');
                });
            }
        };
    }else{
        $location.path('/offline');
    }
});

 
