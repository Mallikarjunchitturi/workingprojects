package com.moverr.vendor;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by MALLIKARJUN on 12/21/2017.
 */

public class VolleyClass {


     public static void postData(String Url, final JSONObject request, Context listner, final LinearLayout mLayout, RequestQueue requestQueue){

          final PostCommentResponseListener mListner = (PostCommentResponseListener)listner;

          JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                  Request.Method.GET,
                  Url,
                  null,
                  new Response.Listener<JSONObject>() {
                       @Override
                       public void onResponse(JSONObject response) {
                            // Do something with response
                            //mTextView.setText(response.toString());

                            // Process the JSON
                            mListner.requestCompleted(response);

                       }
                  },
                  new Response.ErrorListener(){
                       @Override
                       public void onErrorResponse(VolleyError error){
                            // Do something when error occurred
                            Snackbar.make(
                                    mLayout,
                                    "Error.",
                                    Snackbar.LENGTH_LONG
                            ).show();
                       }
                  }
          ) {
               @Override
               protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    Iterator mIt = request.keys();
                    while(mIt.hasNext()){
                         String key = mIt.next().toString();
                         try {
                              params.put(key,request.get(key).toString());
                         }catch (Exception e){
                              e.printStackTrace();
                         }

                    }

                    return params;
               }
          };

          // Add JsonObjectRequest to the RequestQueue
          requestQueue.add(jsonObjectRequest);
     };

     public interface PostCommentResponseListener {
          public void requestStarted();
          public void requestCompleted(JSONObject mObject);
          public void requestEndedWithError(VolleyError error);
     }
}
