package com.moverr.vendor;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MALLIKARJUN on 12/17/2017.
 */

public class RecentMoves extends Fragment  {

    List<HashMap<String,String>> mListRecentMoves, mListPurchasedMoves;
    HashMap<String,String> mHashMap;
    LinearLayout mLayout;
    MoverListAdapter mAdapter;
    TextView txtVendorName;
    ProgressDialog progress;
    TextView txtPoints, txtSettings, txtAboutUs, txtContactUs, btnBack;
    View mView;

    int typeOfMove = 0;

    ActivityCommunicator activityCommunicator;

   // public static String


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_list, container, false);

        activityCommunicator = (ActivityCommunicator) getActivity();

        mLayout = (LinearLayout) mView.findViewById(R.id.linearLayout);

        RecyclerView mRecyclerView = (RecyclerView)mView. findViewById(R.id.list_recycler);
       /* txtVendorName = (TextView) mView.findViewById(R.id.txtVendorName);

        txtPoints = (TextView) mView.findViewById(R.id.txtPoints);
        txtSettings = (TextView) mView.findViewById(R.id.txtSettings);
        txtAboutUs = (TextView) mView.findViewById(R.id.txtAboutus);
        txtContactUs = (TextView) mView.findViewById(R.id.txtContactUs);*/
        btnBack = (TextView) mView.findViewById(R.id.btnBack);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mListRecentMoves =  new ArrayList<>();
        mListPurchasedMoves =  new ArrayList<>();
        if(getArguments() != null){
            typeOfMove = getArguments().getInt("typeOfMove");
        }
        mAdapter = new MoverListAdapter(mListRecentMoves);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ((MoverListAdapter) mAdapter).setOnItemClickListener(new MoverListAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {

                Toast.makeText(getActivity(),"You Clicked item"+position,Toast.LENGTH_SHORT).show();

                Bundle mBundle = new Bundle();
                mBundle.putString("product_id",mListRecentMoves.get(position).get("ID").toString());
                mBundle.putString("status",mListRecentMoves.get(position).get("status").toString());
                activityCommunicator.passDataToActivity("itemDetails",mBundle);

            }
        });
       // txtVendorName.setText(getIntent().getExtras().getString("vendor_name"));

        /*txtPoints.setOnClickListener(this);
        txtSettings.setOnClickListener(this);
        txtAboutUs.setOnClickListener(this);
        txtContactUs.setOnClickListener(this);*/
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommunicator.backPress();
            }
        });
        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";
        if(typeOfMove == 0) {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action","vendorrecentrequets");

            loadData(mServerUrl,params,0);
        }else{
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action","vendorrecentrequets");
            params.put("user_id",""+LoginActivity1.mSharedPreferences.getString("user_id", ""));
            loadData(mServerUrl,params,1);
        }




        return mView;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        context = getActivity();
        // ((DashBoardActivity) context).mFragmentCommunicator = (FragmentCommunicator) this;

    }

    public void loadData(String mUrl, HashMap<String, String> action,final int stat){

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please Wait ! ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();

        RequestQueue requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();





        MultipartRequest multipartRequest =
                new MultipartRequest(mUrl, action, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //[{"ID":"1333","post_title":"Solapur-Dehradun"}]
                        Log.i("response","response:   "+response.toString());
                        progress.dismiss();
                        try{
                            JSONArray mJsonArray = new JSONArray(response);
                            for(int i = 0;i <mJsonArray.length();i++){
                                mHashMap = new HashMap<>();
                                mHashMap.put("ID",mJsonArray.getJSONObject(i).get("ID").toString());
                                mHashMap.put ("post_title",mJsonArray.getJSONObject(i).get("post_title").toString());
                                mHashMap.put("status",""+stat);

                                mListRecentMoves.add(mHashMap);

                            }
                            mAdapter.notifyDataSetChanged();
                            //loadProductStatus();
                        }catch(Exception e){
                            DashBoardActivity.showSnackbar(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        DashBoardActivity.showSnackbar(error.getMessage());
                    }
                });

        requestQueue.add(multipartRequest);

    }

    public void loadProductStatus(){

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please Wait ! ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();

        RequestQueue requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("action","vendorproductstatus");
        params.put("user_id",""+DashBoardActivity.vendor_user_id);

        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";
        MultipartRequest multipartRequest =
                new MultipartRequest(mServerUrl, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // ["1226","1277","1153"]
                        Log.i("response","response:   "+response.toString());
                        progress.dismiss();
                        try{
                            JSONObject mJsonObject = new JSONObject(response);
                           // JSONArray mJsonArray = new JSONArray(response);


                            int j = 0;
                            for(int i = 0;i <mListRecentMoves.size();i++){

                                for(j=0; j < mJsonObject.getJSONArray("recentproductstatus").length();j++){
                                    if((mJsonObject.getJSONArray("recentproductstatus").get(j).toString()).equals(mListRecentMoves.get(i).get("ID"))){
                                        mListRecentMoves.get(i).put("status","1");


                                    }
                                }

                            }
                            int size = mListRecentMoves.size();
                           for(int i = 0;i<size;i++){
                               if((mListRecentMoves.get(i).get("status")).equals("1")){
                                   mListPurchasedMoves.add(mListRecentMoves.get(i));
                                   mListRecentMoves.remove(mListRecentMoves.get(i));
                               }
                           }

                           if(typeOfMove == 1){
                               mListRecentMoves = mListPurchasedMoves;
                           }
                            mAdapter.notifyDataSetChanged();
                        }catch(Exception e){
                            DashBoardActivity.showSnackbar(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();

                        DashBoardActivity.showSnackbar(error.getMessage());
                    }
                });

        requestQueue.add(multipartRequest);

    }
}
