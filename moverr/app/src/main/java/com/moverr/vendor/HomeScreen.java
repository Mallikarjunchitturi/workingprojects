package com.moverr.vendor;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by MALLIKARJUN on 1/6/2018.
 */

public class HomeScreen extends Fragment implements View.OnClickListener {

    View mView;


    TableRow tbBuyPoints, tbPayNow;
    TextView txtperchasedMovesNumber,txtrecentMovesNumber;
    ActivityCommunicator activityCommunicator;


    ProgressDialog progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_homescreen, container, false);

        activityCommunicator = (ActivityCommunicator) getActivity();

        tbBuyPoints = (TableRow) mView.findViewById(R.id.tbRecentMoves);
        tbPayNow = (TableRow) mView.findViewById(R.id.tbPurchasedMoves);

        txtrecentMovesNumber = (TextView) mView.findViewById(R.id.recentMovesNumber);
        txtperchasedMovesNumber = (TextView) mView.findViewById(R.id.perchasedMovesNumber);



        tbBuyPoints.setOnClickListener(this);
        tbPayNow.setOnClickListener(this);

        loadDashBoardData();

        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // ((DashBoardActivity) context).mFragmentCommunicator = (FragmentCommunicator) this;

    }

    @Override
    public void onClick(View view) {
        Intent mIntent = null;
        Bundle mBundle = new Bundle();

        switch (view.getId()) {
            case R.id.tbRecentMoves:
                mBundle.putInt("typeOfMove",0);
                activityCommunicator.passDataToActivity("recentMoves",mBundle);
                break;
            case R.id.tbPurchasedMoves:
                mBundle.putInt("typeOfMove",1);
                activityCommunicator.passDataToActivity("purchasedMoves",mBundle);
                break;

        }
    }

    public void loadDashBoardData(){
        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please Wait ! ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();

        RequestQueue requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("action","androiddashboard");
        params.put("vendor_id",""+DashBoardActivity.vendor_user_id);

        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";
        MultipartRequest multipartRequest =
                new MultipartRequest(mServerUrl, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //[{"ID":"1333","post_title":"Solapur-Dehradun"}]
                        Log.i("response","response:   "+response.toString());

                        //{"recentcnt":"5","purchasedcnt":"3","status":1}
                        progress.dismiss();
                        try{
                            JSONObject mJsonObject = new JSONObject(response);
                            txtrecentMovesNumber.setText(mJsonObject.get("recentcnt").toString());
                            txtperchasedMovesNumber.setText(mJsonObject.get("purchasedcnt").toString());
                        }catch(Exception e){
                            DashBoardActivity.showSnackbar(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();
                        DashBoardActivity.showSnackbar(error.getMessage());
                    }
                });

        requestQueue.add(multipartRequest);
    }
}
