package com.moverr.vendor;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;

import java.util.HashMap;

public class DashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ActivityCommunicator, View.OnClickListener {

    public FragmentCommunicator mFragmentCommunicator;
    public static LinearLayout mLayout;
    public static String vendorName = "", vendor_user_id = "";
    public static boolean fromFooter = false;
    int rewards = 0;

    TextView txtBuy, txtPay, txtNotifications, txtVendorName;
    FragmentManager fragmentManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        vendorName = LoginActivity1.mSharedPreferences.getString("vendor_name","");
        vendor_user_id =  LoginActivity1.mSharedPreferences.getString("user_id","");
        rewards =  LoginActivity1.mSharedPreferences.getInt("rewards",0);


        mLayout = (LinearLayout) findViewById(R.id.mlinear);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        View hView =  navigationView.getHeaderView(0);
        TextView nav_user = (TextView)hView.findViewById(R.id.txtVendorNamemenu);
        TextView balInfo = (TextView)hView.findViewById(R.id.availBalanceInfo);
        nav_user.setText(vendorName);
        balInfo.setText(rewards+"");



        navigationView.setNavigationItemSelectedListener(this);


        fragmentManager = getFragmentManager();

      //  txtVendorName = (TextView) findViewById(R.id.txtVendorNamemenu);
        txtBuy = (TextView) findViewById(R.id.txtBuy);
        txtPay = (TextView) findViewById(R.id.txtPay);
        // txtNotifications = (TextView) findViewById(R.id.txtNotification);
      //  txtVendorName.setText(""+vendorName);
        txtBuy.setOnClickListener(this);
        txtPay.setOnClickListener(this);
        // txtNotifications.setOnClickListener(this);
        loadFragment("homeScreen", new HomeScreen(), null);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else if(fragmentManager.getBackStackEntryCount() == 1){
            Toast.makeText(this,"press again to exist",Toast.LENGTH_LONG).show();

        } else {
            if (fragmentManager.getBackStackEntryCount() <= 2 ) {
                fromFooter = false;
                moveToHome();
            } else {
                super.onBackPressed();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Bundle mBundle = new Bundle();
        if (id == R.id.home) {
            // Handle the camera action
            // loadFragment("homeScreen", new HomeScreen(), null);
            moveToHome();
        } else if (id == R.id.recentMoves) {
            mBundle.putInt("typeOfMove",0);
            loadFragmentWithBackstackNull("recentMoves", new RecentMoves(), mBundle);
        } else if (id == R.id.purchasedMoves) {
            mBundle.putInt("typeOfMove",1);
            loadFragmentWithBackstackNull("purchasedMoves", new RecentMoves(), mBundle );
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void passDataToActivity(String actionName, Bundle mBundle) {
        switch (actionName) {
            case "homeScree":
                loadFragment(actionName, new HomeScreen(), mBundle);
                break;
            case "recentMoves":
                loadFragment(actionName, new RecentMoves(), mBundle);
                break;
            case "purchasedMoves":
                loadFragment(actionName, new RecentMoves(), mBundle);
                break;
            case "pageBuy":
                loadFragmentWithBackstackNull(actionName, new PointsFragment(), mBundle);
                break;
            /*case "pagePay":
                loadFragment(actionName, new HomeScreen(), mBundle);
                break;
            case "pageNotifications":
                loadFragment(actionName, new HomeScreen(), mBundle);
                break;*/
            case "itemDetails":
                loadFragment(actionName, new MoverListItemDetails(), mBundle);
                break;
            case "pointsDetails":
                loadFragment(actionName, new PointsDetails(), mBundle);
                break;
            case "paynowPage":
                loadFragmentWithBackstackNull(actionName, new PayNowPage(), mBundle);
                break;
        }

    } 

    public void loadFragment(String name, Fragment mFragment, Bundle mBundle) {

        if (mBundle != null) {
            mFragment.setArguments(mBundle);
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.mFramLayout, mFragment);
        fragmentTransaction.addToBackStack(name);
        fragmentTransaction.commit();

    }
    public void loadFragmentWithBackstackNull(String name, Fragment mFragment, Bundle mBundle) {
        fromFooter = true;
        if (mBundle != null) {
            mFragment.setArguments(mBundle);
        }
        int count = fragmentManager.getBackStackEntryCount();
        for(int i = 0; i < count-1; ++i) {
            fragmentManager.popBackStack();
        }
        //fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        /*fragmentManager.beginTransaction().
                remove(getFragmentManager().findFragmentById(R.id.mFramLayout)).commit();*/

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.mFramLayout, mFragment);
        fragmentTransaction.addToBackStack(name);
        fragmentTransaction.commit();

    }

    public static void showSnackbar(String message) {
        Snackbar.make(mLayout, "" + message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        Intent mIntent = null;

        switch (view.getId()) {

            case R.id.txtBuy:
                passDataToActivity("pageBuy", null);
                break;
            case R.id.txtPay:
                passDataToActivity("paynowPage", null);
                break;
            /*case R.id.txtNotification:
                passDataToActivity("pageNotifications");
                break;*/
        }
    }

    @Override
    public void backPress() {

        if (fragmentManager.getBackStackEntryCount() <= 1 ) {
            fromFooter = false;
            moveToHome();
        } else {
            onBackPressed();
        }

    }


    private void moveToHome() {
        Intent mIntent = new Intent(this, DashBoardActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mIntent);
        finish();
    }


}
