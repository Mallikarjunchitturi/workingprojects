package com.moverr.vendor;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by MALLIKARJUN on 1/23/2018.
 */

public class PointsFragment  extends Fragment implements View.OnClickListener {

    View mView;

TextView btnBack,txtSelectedPoints;
    TableRow tbRecentMoves, tbPurchasedMoves;
    RelativeLayout mRelativeLayout1, mRelativeLayout2, mRelativeLayout3, mRelativeLayout4, mRelativeLayout5, mRelativeLayout6;
    ActivityCommunicator activityCommunicator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.activity_points, container, false);

        activityCommunicator = (ActivityCommunicator) getActivity();

        mRelativeLayout1 = (RelativeLayout) mView.findViewById(R.id.points_relative1);
        mRelativeLayout2 = (RelativeLayout) mView.findViewById(R.id.points_relative2);
        mRelativeLayout3 = (RelativeLayout) mView.findViewById(R.id.points_relative3);
        mRelativeLayout4 = (RelativeLayout) mView.findViewById(R.id.points_relative4);
        mRelativeLayout5 = (RelativeLayout) mView.findViewById(R.id.points_relative5);
        mRelativeLayout6 = (RelativeLayout) mView.findViewById(R.id.points_relative6);

        btnBack = (TextView) mView.findViewById(R.id.btnBack);


        mRelativeLayout1.setOnClickListener(this);
        mRelativeLayout2.setOnClickListener(this);
        mRelativeLayout3.setOnClickListener(this);
        mRelativeLayout4.setOnClickListener(this);
        mRelativeLayout5.setOnClickListener(this);
        mRelativeLayout6.setOnClickListener(this);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommunicator.backPress();
            }
        });

       /* tbRecentMoves = (TableRow) mView.findViewById(R.id.tbRecentMoves);
        tbPurchasedMoves = (TableRow) mView.findViewById(R.id.tbPurchasedMoves);



        tbRecentMoves.setOnClickListener(this);
        tbPurchasedMoves.setOnClickListener(this);
*/


        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        context = getActivity();
        // ((DashBoardActivity) context).mFragmentCommunicator = (FragmentCommunicator) this;

    }

    @Override
    public void onClick(View view) {
        Intent mIntent = null;
        Bundle mBundle = new Bundle();
        switch (view.getId()) {
            case R.id.points_relative1:
                mBundle.putInt("points",50);
                mBundle.putInt("SP",45);
                break;
            case R.id.points_relative2:
                mBundle.putInt("points",100);
                mBundle.putInt("SP",90);
                break;
            case R.id.points_relative3:
                mBundle.putInt("points",200);
                mBundle.putInt("SP",190);
                break;
            case R.id.points_relative4:
                mBundle.putInt("points",300);
                mBundle.putInt("SP",250);
                break;
            case R.id.points_relative5:
                mBundle.putInt("points",400);
                mBundle.putInt("SP",350);
                break;
            case R.id.points_relative6:
                mBundle.putInt("points",500);
                mBundle.putInt("SP",400);
                break;

        }
        activityCommunicator.passDataToActivity("pointsDetails",mBundle);
    }


}

