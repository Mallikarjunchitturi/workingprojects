package com.moverr.vendor;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

/**
 * Created by MALLIKARJUN on 12/17/2017.
 */

public class MoverListItemDetailsAdapter extends RecyclerView.Adapter<MoverListAdapter.DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    // private static MyClickListener myClickListener;
    List<HashMap<String, String>> mDataset;
    private static MoverListAdapter.MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder  implements View.OnClickListener       {
        TextView txt_list_Ordernumber;
        TextView txt_to_from;
        ImageView img_read_status;


        public DataObjectHolder(View itemView) {
            super(itemView);
            txt_list_Ordernumber = (TextView) itemView.findViewById(R.id.txt_list_Ordernumber);
            txt_to_from = (TextView) itemView.findViewById(R.id.txt_to_from);
            img_read_status = (ImageView) itemView.findViewById(R.id.img_read_status);

            Log.i(LOG_TAG, "Adding Listener");
            //itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            myClickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public MoverListItemDetailsAdapter(List<HashMap<String, String>> myDataset) {
        mDataset = myDataset;
    }
    public void setOnItemClickListener(MoverListAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public MoverListAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items, parent, false);

        MoverListAdapter.DataObjectHolder dataObjectHolder = new MoverListAdapter.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(MoverListAdapter.DataObjectHolder holder, int position) {
        holder.txt_list_Ordernumber.setText("");
        holder.txt_to_from.setText("");
        holder.img_read_status.setImageResource(R.drawable.ic_menu_camera);
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}


