package com.moverr.vendor;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by MALLIKARJUN on 12/17/2017.
 */

public class MoverListItemDetails extends Fragment {

    TextView txt_party_Name, txtFrom_To, txtOrderNumber, txt_party_Number, txtTravelDate;
   // ImageView img_status;
    TextView txt_movingTypeValue;
    TextView txtFrom_City, txtFrom_Area, txtFrom_Pin, txtFrom_Floor, txtFrom_Lift;
    TextView txtTo_City, txtTo_Area, txtTo_Pin, txtTo_Floor, txtTo_Lift;
    TextView txtBuyData, btnBack;
    ProgressDialog progress;
    TableRow bottomTableRow;
    String productId = "";
    LinearLayout mLayout;
    View mView;

    ActivityCommunicator activityCommunicator;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.item_details, container, false);

        btnBack = (TextView) mView.findViewById(R.id.btnBack);

        txtBuyData = (TextView) mView.findViewById(R.id.txt_buy_data);
        txt_party_Number = (TextView) mView.findViewById(R.id.txt_party_Number);
        txt_party_Name = (TextView) mView.findViewById(R.id.txt_party_Name);


        txtOrderNumber = (TextView) mView.findViewById(R.id.txt_list_Ordernumber);
        txtFrom_To = (TextView) mView.findViewById(R.id.txt_from_to);
        txtTravelDate = (TextView) mView.findViewById(R.id.txt_date);
        //img_status = (ImageView) mView.findViewById(R.id.img_status);
        txt_movingTypeValue = (TextView) mView.findViewById(R.id.txt_movingTypeValue);

        txtFrom_City = (TextView) mView.findViewById(R.id.txt_movingfrom_cityValue);
        txtFrom_Area = (TextView) mView.findViewById(R.id.txt_movingfrom_areaValue);
        txtFrom_Pin = (TextView) mView.findViewById(R.id.txt_movingfrom_pinValue);
        txtFrom_Floor = (TextView) mView.findViewById(R.id.txt_movingfrom_floorValue);
        txtFrom_Lift = (TextView) mView.findViewById(R.id.txt_movingfrom_liftValue);

        txtTo_City = (TextView) mView.findViewById(R.id.txt_movingto_cityValue);
        txtTo_Area = (TextView) mView.findViewById(R.id.txt_movingto_areaValue);
        txtTo_Pin = (TextView) mView.findViewById(R.id.txt_movingto_pinValue);
        txtTo_Floor = (TextView) mView.findViewById(R.id.txt_movingto_floorValue);
        txtTo_Lift = (TextView) mView.findViewById(R.id.txt_movingto_liftValue);
        bottomTableRow = (TableRow) mView.findViewById(R.id.tableRowBottom);

        mLayout = (LinearLayout) mView.findViewById(R.id.mLinearLayout);
        if(getArguments().getString("status").equals("1")){
            bottomTableRow.setVisibility(View.VISIBLE);
            txtBuyData.setVisibility(View.GONE);
        }else{
            bottomTableRow.setVisibility(View.GONE);
            txtBuyData.setVisibility(View.VISIBLE);
        }
        loadProductDetails();

        activityCommunicator = (ActivityCommunicator) getActivity();
        txtBuyData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postDataToServer();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCommunicator.backPress();
            }
        });

        return mView;
    }

    public void loadProductDetails() {

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please Wait ! ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();

        RequestQueue requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("action", "singleproductdata");
        params.put("user_id", "" + LoginActivity1.mSharedPreferences.getString("user_id", ""));
        params.put("product_id", "" + getArguments().getString("product_id"));

        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";

        MultipartRequest multipartRequest =
                new MultipartRequest(mServerUrl, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("response", "response:   " + response.toString());
                        progress.dismiss();
                        try {


                            JSONObject mJsonObject = new JSONObject(response);
                            // txtVendorName.setText(""+getIntent().getExtras().get("vendor_name"));
                            txtOrderNumber.setText("" + mJsonObject.get("product_code"));
                            productId = "" + mJsonObject.get("product_code");
                            txt_party_Number.setText("" + mJsonObject.get("user_phone"));
                            txt_party_Name.setText("" + mJsonObject.get("user_name"));
                            txtTravelDate.setText("" + mJsonObject.get("traveldate"));
                            txtFrom_To.setText(mJsonObject.get("from_city") + " - " + mJsonObject.get("to_city"));

                            txt_movingTypeValue.setText("" + mJsonObject.get("wcv_shifttype"));

                            txtFrom_City.setText("" + mJsonObject.get("from_city"));
                            txtFrom_Area.setText("" + mJsonObject.get("from_area"));
                            txtFrom_Pin.setText("" + mJsonObject.get("from_postalcode"));
                            // txtFrom_Floor.setText(""+mJsonObject.get("from_postalcode"));
                            // txtFrom_Lift = (TextView) findViewById(R.id.txt_movingfrom_liftValue);

                            txtTo_City.setText("" + mJsonObject.get("to_city"));
                            txtTo_Area.setText("" + mJsonObject.get("to_area"));
                            txtTo_Pin.setText("" + mJsonObject.get("to_postalcode"));
                            //txtTo_Floor = (TextView) findViewById(R.id.txt_movingto_floorValue);
                            //txtTo_Lift = (TextView) findViewById(R.id.txt_movingto_liftValue);


                        } catch (Exception e) {
                            e.printStackTrace();
                            //Snackbar.make(mLayout,""+e.getMessage(),Snackbar.LENGTH_LONG).show();

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();

                        // Snackbar.make(mLayout,""+error.getMessage(),Snackbar.LENGTH_LONG).show();
                    }
                });

        requestQueue.add(multipartRequest);

    }

    public void postDataToServer() {


        progress = new ProgressDialog(getActivity());
        progress.setMessage("Please Wait ! ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();

        RequestQueue requestQueue = VolleySingleton.getInstance(getActivity()).getRequestQueue();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("action", "productbuy");
        params.put("user_id", "" + LoginActivity1.mSharedPreferences.getString("user_id", ""));
        params.put("product_id", "" + productId);


        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";
        MultipartRequest multipartRequest =
                new MultipartRequest(mServerUrl, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // ["1226","1277","1153"]
                        Log.i("response", "response:   " + response.toString());
                        progress.dismiss();
                        JSONObject mJsonObject = null;
                        try {
                            mJsonObject = new JSONObject(response);
                            bottomTableRow.setVisibility(View.VISIBLE);
                            txtBuyData.setVisibility(View.GONE);
                            txt_party_Number.setText("" + mJsonObject.get("usermobile"));
                            txt_party_Name.setText("" + mJsonObject.get("username"));
                        } catch (Exception e) {
                            Snackbar.make(mLayout, "" + e.getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                        /*Intent mIntent = new Intent(getActivity(), DashBoardActivity.class);
                        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(mIntent);*/
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();

                        Snackbar.make(mLayout, "try again ! .." + error.getMessage(), Snackbar.LENGTH_LONG).show();
                    }
                });

        requestQueue.add(multipartRequest);
    }

    public void showDialog() {

    }
}
