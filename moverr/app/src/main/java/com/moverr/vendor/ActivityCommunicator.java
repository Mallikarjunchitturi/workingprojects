package com.moverr.vendor;

import android.os.Bundle;

/**
 * Created by MALLIKARJUN on 1/6/2018.
 */

public interface ActivityCommunicator {
    public void passDataToActivity(String someValue, Bundle mBundle);
    public void backPress();
}
