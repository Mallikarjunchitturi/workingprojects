package com.moverr.vendor;

/**
 * Created by MALLIKARJUN on 1/6/2018.
 */

public interface FragmentCommunicator {
    public void passDataToFragment(String someValue);
}
