package com.moverr.vendor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;


import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by 10620566 on 12/13/2017.
 */

public class LoginActivity1 extends Activity implements VolleyClass.PostCommentResponseListener {

    EditText edtUsername,edtPassword;
    RelativeLayout mLayout;
    ProgressDialog progress;
    public static SharedPreferences mSharedPreferences;

    private static final String TAG = LoginActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sign_in_fragment);

        edtUsername = (EditText) findViewById(R.id.edt_username);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        ImageView mm = (ImageView) findViewById(R.id.img);
        mLayout = (RelativeLayout) findViewById(R.id.layout);

        mSharedPreferences = getSharedPreferences("Moverr",MODE_PRIVATE);
        if(MyUtility.checkNetworkStatus(this)) {
           registerBroadcast();
        }else{
            MyUtility.showToastMsg(this,"Please Connect to Network...");
        }

    }


    public void validateFields(View v){
        Toast.makeText(this,"signIn",Toast.LENGTH_LONG).show();
        attemptLogin();

    }
    public void moveToSignup(View v){
        Toast.makeText(this,"signIn",Toast.LENGTH_LONG);
        Intent mIntent = new Intent(this,SignupActivity.class);
        finish();
        startActivity(mIntent);
    }

    private void attemptLogin() {

        // Reset errors.
        edtUsername.setError(null);
        edtPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            edtPassword.setError(getString(R.string.error_invalid_password));
            focusView = edtPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            edtUsername.setError(getString(R.string.error_field_required));
            focusView = edtUsername;
            cancel = true;
        } else if ((!isEmailValid(email) && (!isMobileValid(email)))) {
            edtUsername.setError(getString(R.string.error_invalid_email));
            focusView = edtUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

            postData(email,password);

        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
       // return email.contains("@");
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isMobileValid(String number) {
        //TODO: Replace this with your own logic

        return Patterns.PHONE.matcher(number).matches();
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }



    @Override
    public void requestStarted() {

    }

    @Override
    public void requestCompleted(JSONObject mObject) {

        System.out.print(mObject.toString());

    }

    @Override
    public void requestEndedWithError(VolleyError error) {

    }
    public void postData(String email, String password){
        progress = new ProgressDialog(this);
        progress.setMessage("Please Wait ! ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();
        RequestQueue requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("action","androidapplogin");
        params.put("user_name",email);
        params.put("user_pass",password);
        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";
        MultipartRequest multipartRequest =
                new MultipartRequest(mServerUrl, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progress.dismiss();
                     //   {"user_id":"184","user_login":"sunil.n","user_email":"sunil.n@liefr.com","rewards":"50","status":1}
                        Log.i("response","response:   "+response.toString());
                        try {
                            JSONObject mJsonObject = new JSONObject(response);
                            Intent mIntent = new Intent(LoginActivity1.this,  DashBoardActivity.class);
                           SharedPreferences.Editor  mEditor =  mSharedPreferences.edit();
                           mEditor.putString("vendor_name", "" + mJsonObject.get("user_login").toString());
                           // mIntent.putExtra("vendor_name", "" + mJsonObject.get("user_login").toString());
                            mEditor.putString("user_id", "" + mJsonObject.get("user_id").toString());
                            mEditor.putInt("rewards", Integer.parseInt(mJsonObject.get("rewards").toString()));
                            mEditor.commit();
                            finish();
                            startActivity(mIntent);
                        }catch (Exception e){
                            Snackbar.make(mLayout,""+e.getMessage(),Snackbar.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.dismiss();

                        Snackbar.make(mLayout,""+error.getMessage(),Snackbar.LENGTH_LONG).show();

                    }
                });

        requestQueue.add(multipartRequest);
    }

    public void  registerBroadcast(){
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications

                    displayFirebaseRegId(intent.getStringExtra("token"));
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                   // displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //  txtMessage.setText(message);
                }
            }
        };

       // displayFirebaseRegId();

    }
    public void displayFirebaseRegId(String token){
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String userId = mSharedPreferences.getString("user_id",null);
        if(userId  != null)
            postFCMData(token,userId);

    }

    //
    public void postFCMData(String regId,String userId){

        RequestQueue requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("action","androidfcmupdation");
        params.put("app_id",regId);
        params.put("user_id",userId);
        params.put("app_key","AIzaSyCJfOMf1yyr9P6YyHfKO2resD_vJskYHTA");

        String mServerUrl = "http://staging114.liefr.com/wp-admin/admin-ajax.php";
        MultipartRequest multipartRequest =
                new MultipartRequest(mServerUrl, params, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //   {"user_id":"184","user_login":"sunil.n","user_email":"sunil.n@liefr.com","rewards":"50","status":1}
                       // Log.i("response","response:   "+response.toString());
                        try {
                            JSONObject mJsonObject = new JSONObject(response);

                        }catch (Exception e){
                            Snackbar.make(mLayout,""+e.getMessage(),Snackbar.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Snackbar.make(mLayout,""+error.getMessage(),Snackbar.LENGTH_LONG).show();

                    }
                });

        requestQueue.add(multipartRequest);
    }


}
