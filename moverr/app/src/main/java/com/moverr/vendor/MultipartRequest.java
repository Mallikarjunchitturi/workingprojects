package com.moverr.vendor;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by MALLIKARJUN on 12/22/2017.
 */

public class MultipartRequest extends Request<String> {

    private HttpEntity mHttpEntity;
    private MultipartEntityBuilder builder = MultipartEntityBuilder.create();

    private final Response.Listener<String> mListener;
    private final Map<String, String> mKeyValue;

    public MultipartRequest(String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);

        mListener = listener;
        mKeyValue = params;
        buildMultipartEntity();
    }

    private void buildMultipartEntity() {

        for (Map.Entry<String, String> entry : mKeyValue.entrySet()) {
            builder.addTextBody(entry.getKey(), entry.getValue());
        }

        mHttpEntity = builder.build();
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String jsonString = "";
        JSONObject mJsonObject = null;
        try {
            jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
           // mJsonObject = new JSONObject(jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.success(jsonString, getCacheEntry());
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}