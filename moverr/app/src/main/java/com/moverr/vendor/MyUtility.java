package com.moverr.vendor;

import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by MALLIKARJUN on 2/7/2018.
 */

public class MyUtility {

    public static boolean checkNetworkStatus(Context mContext){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static void showToastMsg(Context mContext,String msg){
        Toast.makeText(mContext, ""+msg, Toast.LENGTH_SHORT).show();
    }
}
