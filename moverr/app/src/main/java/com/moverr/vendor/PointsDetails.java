package com.moverr.vendor;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by MALLIKARJUN on 1/26/2018.
 */

public class PointsDetails extends Fragment implements View.OnClickListener{

    View mView;
    ImageView mPointsImage;
    Button btnPlus, btnMinus, btnAddItem;
    EditText edtNumber;
    TextView txtOriginalPrice, txtOfferPrice, mTotalAmt, btnback, txtSelectedPoints;
    int pointsDetails, offeredPrice;

    ActivityCommunicator activityCommunicator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_points_details, container, false);

        mPointsImage = (ImageView) mView.findViewById(R.id.imgPointsDetails);
        btnPlus = (Button) mView.findViewById(R.id.btnPlus);
        btnMinus = (Button) mView.findViewById(R.id.btnMinus);
        btnAddItem = (Button) mView.findViewById(R.id.btnAdd);
        edtNumber = (EditText) mView.findViewById(R.id.edtCount);

        txtSelectedPoints = (TextView)mView.findViewById(R.id.txtSelectedPoints);
        txtOriginalPrice = (TextView) mView.findViewById(R.id.txtOriginalPrice);
        txtOfferPrice = (TextView) mView.findViewById(R.id.txtOfferPrice);
        mTotalAmt = (TextView) mView.findViewById(R.id.txtTotalAmnt);
        btnback = (TextView) mView.findViewById(R.id.btnBack);

        btnPlus.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnAddItem.setOnClickListener(this);
        btnback.setOnClickListener(this);
        edtNumber.setText("1");

        activityCommunicator = (ActivityCommunicator) getActivity();

        if(getArguments() != null){
            pointsDetails =  getArguments().getInt("points");
            offeredPrice = getArguments().getInt("SP");
        }
        txtSelectedPoints.setText(pointsDetails+" Points");
        txtOriginalPrice.setText("Regular Price:"+pointsDetails+"/- Rs");
        txtOfferPrice.setText("Regular Price:"+offeredPrice+"/- Rs");
        if(pointsDetails == 50){
            mPointsImage.setImageResource(R.drawable.png50);
        }else if(pointsDetails == 100){
            mPointsImage.setImageResource(R.drawable.png100);
        }else if(pointsDetails == 200){
            mPointsImage.setImageResource(R.drawable.png200);
        }else if(pointsDetails == 300){
            mPointsImage.setImageResource(R.drawable.png300);
        }else if(pointsDetails == 400){
            mPointsImage.setImageResource(R.drawable.png400);
        }else if(pointsDetails == 500){
            mPointsImage.setImageResource(R.drawable.png500);
        }
        mTotalAmt.setText(""+(1*offeredPrice));
        return mView;
    }

    @Override
    public void onClick(View v) {
        int edtCount = Integer.parseInt(edtNumber.getText().toString());
        switch (v.getId()){
            case R.id.btnAdd:
                mTotalAmt.setText(""+(edtCount*offeredPrice));
                Bundle mBundle = new Bundle();
                mBundle.putInt("totalAmt",(edtCount*offeredPrice));
                mBundle.putInt("points",offeredPrice);
                mBundle.putInt("edtCount",edtCount);
                SharedPreferences.Editor mEditor = LoginActivity1.mSharedPreferences.edit();
                mEditor.putInt("totalAmt",(edtCount*offeredPrice));
                mEditor.putInt("points",offeredPrice);
                mEditor.putInt("edtCount",edtCount);
                mEditor.commit();
                activityCommunicator.passDataToActivity("paynowPage",mBundle);
                break;
            case R.id.btnPlus:
                edtCount = edtCount + 1;
                edtNumber.setText(""+edtCount);
                mTotalAmt.setText(""+(edtCount*offeredPrice));
                break;
            case R.id.btnMinus:
                if(edtCount <=1){
                    edtNumber.setText("1");
                }else{
                    edtCount = edtCount - 1;
                    edtNumber.setText(""+edtCount);
                    mTotalAmt.setText(""+(edtCount*offeredPrice));
                }
                break;
            case R.id.btnBack:
                activityCommunicator.backPress();
                break;
        }
    }
}
