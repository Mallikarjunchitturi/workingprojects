package com.moverr.vendor;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by MALLIKARJUN on 1/27/2018.
 */

public class PayNowPage extends Fragment implements View.OnClickListener{

    View mView;

    TextView txtPriceOfCard, txtNumberOfCards,txtTotalAmnt, btnBack;
    Button btnPayNow;

    ActivityCommunicator activityCommunicator;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_paynow_details, container, false);


        int totalAmnt = 0;
        int pointNumber = 0;
        int edtCount = 0;

        if (getArguments() != null) {
            totalAmnt = getArguments().getInt("totalAmt");
            pointNumber = getArguments().getInt("points");
            edtCount = getArguments().getInt("edtCount");
        } else {
            totalAmnt = LoginActivity1.mSharedPreferences.getInt("totalAmt", 0);
            pointNumber = LoginActivity1.mSharedPreferences.getInt("points", 0);
            edtCount = LoginActivity1.mSharedPreferences.getInt("edtCount", 0);
        }
        activityCommunicator = (ActivityCommunicator) getActivity();

        txtPriceOfCard = (TextView) mView.findViewById(R.id.txtPriceOfCard);
        txtNumberOfCards = (TextView) mView.findViewById(R.id.txtNumberOfCards);
        txtTotalAmnt = (TextView) mView.findViewById(R.id.txtTotalAmnt);
        btnBack = (TextView) mView.findViewById(R.id.btnBack);
        btnPayNow = (Button) mView.findViewById(R.id.btnPayNow);

        txtPriceOfCard.setText(""+pointNumber);
        txtNumberOfCards.setText(""+edtCount);
        txtTotalAmnt.setText(""+totalAmnt);

        btnBack.setOnClickListener(this);
        btnPayNow.setOnClickListener(this);


        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnPayNow:
                // move to payment gateway
                break;
            case R.id.btnBack:
                activityCommunicator.backPress();
                break;
        }
    }
}


